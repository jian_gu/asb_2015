/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var block2 = $('#div-detail-block-2');
    var block2Contents = $('#div-detail-block-2-contents');
    var block2ContentsHeader2 = $('#div-detail-block-2-contents-header-2');

    dynamicHeightBlock3SrchForm($(window).width(), $(block2).height());

    // Resize window, trigger function
    $(window).resize(function() {
        $(block2Contents).height(0);
        var windowWidth = $(window).width();
        var blockHeight = $(block2).height();
        dynamicHeightBlock3SrchForm(windowWidth, blockHeight);
    });

    /**
     * dynamicly resize search form height
     * @param {type} windowWidth
     * @param {type} blockHeight
     * @returns {undefined}
     */
    function dynamicHeightBlock3SrchForm(windowWidth, blockHeight) {
//        console.log(windowWidth);
        // Make contents height to auto
        $(block2Contents).height('auto');
        // Make contents header 2 div's height to auto
        $(block2ContentsHeader2).height('auto');
        // window width > middle size
        if (windowWidth > 991) {
            var newHeight = parseInt(blockHeight);
            $(block2ContentsHeader2).height(newHeight);
        }
    }
});
