/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var isSent = false;

    var contactForm = $('form[name="frm_contact_email"]');
    var btnSend = $(contactForm).find('button#formContactBtnEnvoyer');

    $(contactForm).on('submit', function() {
        event.preventDefault();
        // lock button
        lockModalDiv('on');
        // send mail
        ajaxSendMail();
    });

    function lockModalDiv(indicator) {
        switch (indicator) {
            case 'on':
                if (false === isSent) {
                    $(btnSend).attr('disabled', 'disabled');
                    $(btnSend).html('envoie...');
                }
                break;
            case 'off':
                if (true === isSent) {
                    $(btnSend).html('Envoyé');
                    $(btnSend).removeAttr('disabled');
                    $(btnSend).on("click", function() {
                        event.preventDefault();
                    });
                }
                if (false === isSent) {
                    $(btnSend).html('Envoyer');
                    $(btnSend).removeAttr('disabled');
                }
            default :
                break;
        }
    }

    function ajaxSendMail() {
        var nom = $('#formContactNom').val();
        var prenom = $('#formContactPrenom').val();
        var email = $('#formContactEmail').val();
        var object = $('#formContactObject').val();
        var message = $('#formContactMessage').val();
        var captchaResponse = $('textarea#g-recaptcha-response').val();

        var successed = false;
        var error;
        var msg = "";
        var url = Routing.generate('africaschoolbus_view_contact_email_process');
        $.ajax({
            type: "POST",
            url: url,
            data: {
                nom: nom,
                prenom: prenom,
                email: email,
                object: object,
                message: message,
                g_recaptcha_response: captchaResponse
            },
            cache: true,
            async: true,
            dataType: "json",
            success: function(data)
            {
                successed = data.success;
                error = data.error;
                msg = data.msg;
            },
            complete: function(data) {
                if (successed === true && error === false) {
                    isSent = true;
                    // show message
                    showReturnedMsg(msg);
//                    // active button 
                    lockModalDiv('off');
                }
                if (successed === false && error === true) {
                    isSent = false;
                    // show message
                    showReturnedMsg(msg);
//                    // active button 
                    lockModalDiv('off');
                }
            },
            error: function(xhr, status, error) {
                console.log(status + '; ' + error);
            }
        });
    }

    function showReturnedMsg(msg) {
        // Remove returned masseages
        $('.returned-msg').remove();
        switch (msg) {
            case 'no-captcha':
                // Construct content
                var contents = "<h5 class='returned-msg no-captcha' style='color:red;'>Vous devez enseigner le Captcha!</h5>";
                // Show no captcha message
                $('#label-asb_contact_email_captcha').after(contents);
                break;
            case 'sent':
                // Construct content
                var contents = "<h4 class='returned-msg sent text-uppercase' style='color: #f7941e;'>votre demande a été bien envoyée.</h4>";
                // Show sent message
                $('#asb_contact_email_returned_message').append(contents);
            default :
                break;
        }
    }


});
