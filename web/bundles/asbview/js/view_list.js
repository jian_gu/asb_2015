/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var block3 = $('#div-list-block-3');
    var block3SrchForm = $('#div-list-block-3-contents-frm-srch');
    var addrStr = '';
    var geocoder;                                                               // Map var for Google Map
    var map;                                                                    // Map var for Google Map

    dynamicHeightBlock3SrchForm($(window).width(), $(block3).height());
    mapModalEvents();

    // Resize window, trigger function
    $(window).resize(function() {
        $(block3SrchForm).height(0);
        var windowWidth = $(window).width();
        var blockHeight = $(block3).height();
        dynamicHeightBlock3SrchForm(windowWidth, blockHeight);
    });

    /**
     * dynamicly resize search form height
     * @param {type} windowWidth
     * @param {type} blockHeight
     * @returns {undefined}
     */
    function dynamicHeightBlock3SrchForm(windowWidth, blockHeight) {
        if (windowWidth >= 768) {
            var newHeight = parseInt(blockHeight) + parseInt(90);
            $(block3SrchForm).height(newHeight);
        }
    }


    /////////////////////////////////////////
    /////////////// Map modal ///////////////
    /////////////////////////////////////////

    function mapModalEvents() {
        modalShowEvents();
        modalShownEvents();
    }

    /**
     * Get addtrss string from link modal
     * @returns {String}
     */
    function modalShowEvents() {
        //triggered when modal is about to be shown
        $('#map-modal').on('show.bs.modal', function(e) {

            //get data-id attribute of the clicked element
            var link = $(e.relatedTarget);
            addrStr = link.data('address-value');

            //populate the textbox
            $(e.currentTarget).find('h4#myModalLabel').html(addrStr);
            // Assign the link
            $(e.currentTarget).find('a[type="button"]').attr('href', 'https://www.google.com/maps/place/' + addrStr);
        });
    }

    function modalShownEvents() {
        $('#map-modal').on('shown.bs.modal', function(e) {
            setMapStyle();
            initMap();
            AddrToLatLng(addrStr);
        });
    }

    function setMapStyle() {
        var mapCanvas = $('div#map-canvas');
        var canvasContainer = $(mapCanvas).parent('div');
        $(mapCanvas).css('height', "" + $(canvasContainer).width() * 0.5 + "");
    }

    function initMap() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(6.1657032, 19.2582332);
        var mapOptions = {
            zoom: 3,
            center: latlng
        }
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    }

    function AddrToLatLng(address) {
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                map.setZoom(16);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                console.log("Geocode was not successful for the following reason: " + status);
            }
        });
    }
});
