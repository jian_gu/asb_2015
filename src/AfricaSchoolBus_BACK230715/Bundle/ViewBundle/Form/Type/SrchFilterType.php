<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Acme\TaskBundle\Form\DataTransformer\DomaineToIdTransformer;

class SrchFilterType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // ...

        // the "em" is an option that you pass when creating your form. Check out
        // the 3rd argument to createForm in the next code block to see how this
        // is passed to the form (see also configureOptions).
        $entityManager = $options['em'];
        $domaineTransformer = new DomaineToIdTransformer($entityManager);

        // add a normal text field, but add your transformer to it
        $builder->add(
            $builder->create('domaine', 'text')
                ->addModelTransformer($domaineTransformer)
        );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver
                ->setDefaults(array(
                    'data_class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Formation'
                ))
                ->setRequired(array('em'))
                ->setAllowedTypes('em', 'Doctrine\Common\Persistence\ObjectManager');
    }

    /**
     * @return string
     */
    public function getName() {
        return 'africaschoolbus_viewbundle_srch_filter';
    }

}
