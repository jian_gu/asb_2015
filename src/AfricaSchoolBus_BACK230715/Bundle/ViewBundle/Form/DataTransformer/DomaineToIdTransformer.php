<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// src/AfricaSchoolBus/ViewBundle/Form/DataTransformer/DomaineToIdTransformer.php

namespace Acme\TaskBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine;

class DomaineToIdTransformer implements DataTransformerInterface {

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om) {
        $this->om = $om;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $domaine
     * @return string
     */
    public function transform($domaine) {
        if (null === $domaine) {
            return "";
        }

        return $domaine->getId();
    }

    /**
     * Transforms a string (id) to an object (domaine).
     *
     * @param  string $id
     *
     * @return Issue|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($id) {
        if (!$id) {
            return null;
        }

        $domaine = $this->om
                ->getRepository('ASBDataBundle:Domaine')
                ->findOneBy(array('id' => $id))
        ;

        if (null === $domaine) {
            throw new TransformationFailedException(sprintf(
                    'A domaine with id "%s" does not exist!', $id
            ));
        }

        return $domaine;
    }

}
