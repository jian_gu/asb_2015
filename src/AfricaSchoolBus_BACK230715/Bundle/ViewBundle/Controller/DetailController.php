<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;

class DetailController extends Controller {

    public function IndexAction(Request $request, $formation_id, $formation_title) {
        // Prepare formation infos, for which we need to find
        $formationCriterias = array('id' => $formation_id, 'nom' => $formation_title);
        // Entity manager
        $em = $this->getDoctrine()->getManager();
        // Find formation
        $formation = $em->getRepository('ASBDataBundle:Formation')->findOneBy($formationCriterias);
        // Create keyword search form
        $keywordSrchForm = $this->createKeywordSearchForm();

        return $this->render('ASBViewBundle:Detail:Index.html.twig', array(
                    'keywordSrchForm' => $keywordSrchForm->createView(),
                    'formation' => $formation
        ));
    }

    private function createKeywordSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $textBtnSearch = $this->get('translator')->trans('srch_form.button.search');
        $form = $this->get('form.factory')->createNamedBuilder('srch_form_keyword', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('POST')
                ->add('keyword', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                ))
                ->add('search', 'submit', array(
                    'label' => $textBtnSearch,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

}
