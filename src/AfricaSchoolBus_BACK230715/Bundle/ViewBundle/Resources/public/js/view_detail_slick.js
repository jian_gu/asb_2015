/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    // Get image count
    var imageCount = parseInt($('asb-image-count').attr('data'));

    // Slick slider
    $('.asb-slick').slick({
        dots: true,
        infinite: true,
        slidesToShow: imageCount >= 5 ? parseInt(imageCount) - 1 : imageCount,
        slidesToScroll: imageCount >= 5 ? parseInt(imageCount) - 1 : imageCount,
        asNavFor: '.asb-slick-modal',
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: imageCount >= 3 ? 3 : imageCount,
                    slidesToScroll: imageCount >= 3 ? 3 : imageCount,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: imageCount >= 2 ? 2 : imageCount,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ],
        lazyLoad: 'ondemand',
        centerMode: true,
        focusOnSelect: true
    });

    // Slick modal
    $('.asb-slick-modal').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: false,
        asNavFor: '.asb-slick'
    });

    $('#detail-image-model').on('show.bs.modal', function(event) {

    });
});