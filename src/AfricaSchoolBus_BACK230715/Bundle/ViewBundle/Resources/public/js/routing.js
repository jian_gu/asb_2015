/* 
 * Use FOSJSRouting to facilite the routing path definition, for forms
 * With well formed urls
 * Added by Jian GU
 */


$(document).ready(function() {
    // Define keyword search form element
    var keywordSearchForm = ("form[name='frm_srch_keyword']");
    // Define criterias search form element
    var criteriasSearchForm = ("form[name='frm_srch_criterias']");

    /**
     * Keyword search form submit function
     */
    $(keywordSearchForm).on('submit', function(event) {
        event.preventDefault();
        var keywordElement = $(this).find('input#frm_srch_keyword_keyword');
        var keyword = $.trim(keywordElement.val());
        // Call url with fosjsrouting
//        $.get(Routing.generate('africaschoolbus_view_list_srch_criterias', {keyword: keyword}));
        window.location = Routing.generate('africaschoolbus_view_list_srch_criterias', {keyword: keyword});
        return true;
    });

    /**
     * Criterias search form submit function
     */
    $(criteriasSearchForm).on('submit', function(event) {
        event.preventDefault();
        var keywordElement = $(keywordSearchForm).find('input#frm_srch_keyword_keyword');
        var paysElement = $(this).find('select#frm_srch_criterias_pays option:selected');
        var domaineElement = $(this).find('select#frm_srch_criterias_domaine option:selected');
        var diplomeElement = $(this).find('select#frm_srch_criterias_diplome option:selected');
        var modeElement = $(this).find('select#frm_srch_criterias_mode option:selected');
        var keyword = $.trim(keywordElement.val());
        var pays = $.trim(paysElement.val());
        var domaine = $.trim(domaineElement.val());
        var diplome = $.trim(diplomeElement.val());
        var mode = $.trim(modeElement.val());
        var criteriasObject = constructCriteriaObject(keyword, pays, domaine, diplome, mode);
        // Call url with fosjsrouting
        window.location = Routing.generate('africaschoolbus_view_list_srch_criterias', criteriasObject);
        return true;
    });

    /**
     * 
     * @param {type} keyword
     * @param {type} pays
     * @param {type} domaine
     * @param {type} diplome
     * @param {type} mode
     * @returns {_L8.constructCriteriaObject.critiasObject}
     */
    function constructCriteriaObject(keyword, pays, domaine, diplome, mode) {
        var critiasObject = {};
        keyword !== '' ? critiasObject["keyword"] = keyword : null;
        pays !== '' ? critiasObject["pays"] = pays : null;
        domaine !== '' ? critiasObject["domaine"] = domaine : null;
        diplome !== '' ? critiasObject["diplome"] = diplome : null;
        mode !== '' ? critiasObject["mode"] = mode : null;
        return critiasObject;
    }
});