<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ASBDataBundle:Default:index.html.twig', array('name' => $name));
    }
}
