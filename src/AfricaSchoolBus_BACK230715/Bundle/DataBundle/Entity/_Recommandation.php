<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Comment as BaseComment;
use FOS\CommentBundle\Model\ThreadInterface;

/**
 * Recommandation
 *
 * @ORM\Table(name="recommandation")
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Recommandation extends BaseComment {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createAt", type="datetime")
     */
    private $createAt;

    /**
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Formation", inversedBy="recommandations")
     * @ORM\JoinColumn(name="formation_id", referencedColumnName="id")
     */
    private $formation;

    /**
     * @var string
     *
     * @ORM\Column(name="contents", type="text")
     */
    private $contents;

    /**
     * Thread of this comment
     *
     * @var Thread
     * @ORM\ManyToOne(targetEntity="AfricaSchoolBus\Bundle\DataBundle\Entity\RecommandationThread")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    protected $thread;

    /**
     * @param ThreadInterface $thread
     *
     * @return void
     */
    public function setThread(ThreadInterface $thread) {
        $this->thread = $thread;
    }

    public function __construct() {
        
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     * @return Recommandation
     */
    public function setCreateAt($createAt) {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime 
     */
    public function getCreateAt() {
        return $this->createAt;
    }

    /**
     * Set contents
     *
     * @param string $contents
     * @return Recommandation
     */
    public function setContents($contents) {
        $this->contents = $contents;

        return $this;
    }

    /**
     * Get contents
     *
     * @return string 
     */
    public function getContents() {
        return $this->contents;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Recommandation
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set formation
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formation
     * @return Recommandation
     */
    public function setFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formation = null) {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation 
     */
    public function getFormation() {
        return $this->formation;
    }

    /**
     * Get thread
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\RecommandationThread 
     */
    public function getThread() {
        return $this->thread;
    }

}
