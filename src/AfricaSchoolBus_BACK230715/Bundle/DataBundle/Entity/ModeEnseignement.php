<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ModeEnseignement
 *
 * @ORM\Table(name="mode_enseignement")
 * @ORM\Entity
 */
class ModeEnseignement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=255)
     */
    private $mode;
    
    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="modeEnseignement")
     **/
    private $formations;
    
    public function __construct() {
        $this->formations = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getMode();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mode
     *
     * @param string $mode
     * @return ModeEnseignement
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string 
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return ModeEnseignement
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }
}
