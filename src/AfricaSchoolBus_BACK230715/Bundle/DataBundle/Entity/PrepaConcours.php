<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * PrepaConcours
 *
 * @ORM\Table(name="prepa_concours")
 * @ORM\Entity
 */
class PrepaConcours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="concours", type="string", length=255)
     */
    private $concours;

    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="diplome")
     **/
    private $formations;

    public function __construct() {
        $this->formations = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getConcours();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concours
     *
     * @param string $concours
     * @return PrepaConcours
     */
    public function setConcours($concours)
    {
        $this->concours = $concours;

        return $this;
    }

    /**
     * Get concours
     *
     * @return string 
     */
    public function getConcours()
    {
        return $this->concours;
    }

    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return PrepaConcours
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }
}
