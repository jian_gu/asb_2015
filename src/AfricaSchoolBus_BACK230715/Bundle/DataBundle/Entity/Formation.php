<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\CommentBundle\Entity\Thread as BaseThread;
//use Symfony\Component\HttpFoundation\File\File;
//use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Formation
 *
 * @ORM\Table(name="formation")
 * @ORM\Entity(repositoryClass="AfricaSchoolBus\Bundle\DataBundle\Entity\FormationRepository")
 */
class Formation extends BaseThread
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=128)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1024)
     */
    private $description;
    
    /**
     * @var string
     *
     * @ORM\Column(name="objectif", type="string", length=255)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="publicVise", type="string", length=255)
     */
    private $publicVise;

    /**
     * @var string
     *
     * @ORM\Column(name="programme", type="text", nullable=true)
     */
    private $programme;

    /**
     * @var string
     *
     * @ORM\Column(name="motDuDirecteur", type="string", length=512, nullable=true)
     */
    private $motDuDirecteur;

    /**
     * @var string
     *
     * @ORM\Column(name="contactNom", type="string", length=64)
     */
    private $contactNom;

    /**
     * @var string
     *
     * @ORM\Column(name="contactAdresse", type="string", length=255)
     */
    private $contactAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="contactEmail", type="string", length=128)
     */
    private $contactEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="contactTelephone", type="string", length=32)
     */
    private $contactTelephone;

    /**
     * @var string
     *
     * @ORM\Column(name="contactSiteWeb", type="string", length=128, nullable=true)
     */
    private $contactSiteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="video_link", type="string", length=512, nullable=true)
     */
    private $videoLink;

    /**
     * @var string
     *
     * @ORM\Column(name="duree", type="string", length=32)
     */
    private $duree;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cout", type="decimal")
     */
    private $cout;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", options={"default" = 1})
     */
    private $activated;

    /**
     * @var decimal
     *
     * @ORM\Column(name="coefficient", type="decimal", options={"default" = 0})
     */
    private $coefficient;

    
    ////////////////////////////////
    ///////// ManyToOne ////////////
    ////////////////////////////////
    /**
     * @ORM\ManyToOne(targetEntity="Diplome", inversedBy="formations")
     * @ORM\JoinColumn(name="diplome_id", referencedColumnName="id")
     * */
    private $diplome;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement", inversedBy="formations")
     * @ORM\JoinColumn(name="etablissement_id", referencedColumnName="id")
     * */
    private $etablissement;

    /**
     * @ORM\ManyToOne(targetEntity="ModeEnseignement", inversedBy="formations")
     * @ORM\JoinColumn(name="mode_enseignement_id", referencedColumnName="id")
     * */
    private $modeEnseignement;

    /**
     * @ORM\ManyToOne(targetEntity="Domaine", inversedBy="formations")
     * @ORM\JoinColumn(name="domaine_id", referencedColumnName="id")
     * */
    private $domaine;

    /**
     * @ORM\ManyToOne(targetEntity="Matiere", inversedBy="formations")
     * @ORM\JoinColumn(name="matiere_id", referencedColumnName="id")
     * */
    private $matiere;

    ////////////////////////////////
    //// OneToMany with join tb/////
    ////////////////////////////////    
    /**
     * @ORM\ManyToMany(targetEntity="FormationImage", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="formation_formation_images",
     *      joinColumns={@ORM\JoinColumn(name="formaiton_image_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="formation_image_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $images;    
    
    /**
     * @ORM\ManyToMany(targetEntity="Session", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="formation_sessions",
     *      joinColumns={@ORM\JoinColumn(name="formaiton_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="session_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $sessions;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;
    
    ////////////////////////////////
    //////////// OneToOne //////////
    //////////////////////////////// 
    /**
     * @ORM\OneToOne(targetEntity="FormationVideo", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="formation_video_id", referencedColumnName="id")
     **/
    private $video;

//    /////// file upload ////////
//    ////////////////////////////
//    /**
//     * NOTE: This is not a mapped field of entity metadata, just a simple property.
//     * 
//     * @Vich\UploadableField(mapping="formation_photo", fileNameProperty="photoName")
//     * 
//     * @var File $photoFile
//     */
//    private $photoFile;
//
//    /**
//     * @ORM\Column(type="string", length=255, name="photo_name", nullable=true)
//     *
//     * @var string $photoName
//     */
//    private $photoName;
//
//    /**
//     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
//     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
//     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
//     * must be able to accept an instance of 'File' as the bundle will inject one here
//     * during Doctrine hydration.
//     *
//     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $photo
//     */
//    public function setPhotoFile(File $photo = null) {
//        $this->photoFile = $photo;
//
//        if ($photo) {
//            // It is required that at least one field changes if you are using doctrine
//            // otherwise the event listeners won't be called and the file is lost
//        $this->setUpdateAt(new \DateTime('now'));
//        }
//    }
//
//    /**
//     * @return File
//     */
//    public function getPhotoFile() {
//        return $this->photoFile;
//    }
//
//    /**
//     * Set photoName
//     *
//     * @param string $photoName
//     * @return formation
//     */
//    public function setPhotoName($photoName) {
//        $this->photoName = $photoName;
//
//        return $this;
//    }
//
//    /**
//     * Get photoName
//     *
//     * @return string 
//     */
//    public function getPhotoName() {
//        return $this->photoName;
//    }
//    
//    /**
//     * NOTE: This is not a mapped field of entity metadata, just a simple property.
//     * 
//     * @Vich\UploadableField(mapping="formation_video", fileNameProperty="videoName")
//     * 
//     * @var File $videoFile
//     */
//    private $videoFile;
//
//    /**
//     * @ORM\Column(type="string", length=255, name="video_name", nullable=true)
//     *
//     * @var string $videoName
//     */
//    private $videoName;
//
//    /**
//     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
//     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
//     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
//     * must be able to accept an instance of 'File' as the bundle will inject one here
//     * during Doctrine hydration.
//     *
//     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $video
//     */
//    public function setVideoFile(File $video = null) {
//        $this->videoFile = $video;
//
//        if ($video) {
//            // It is required that at least one field changes if you are using doctrine
//            // otherwise the event listeners won't be called and the file is lost
//        $this->setUpdateAt(new \DateTime('now'));
//        }
//    }
//
//    /**
//     * @return File
//     */
//    public function getVideoFile() {
//        return $this->videoFile;
//    }
//
//    /**
//     * Set videoName
//     *
//     * @param string $videoName
//     * @return Formation
//     */
//    public function setVideoName($videoName) {
//        $this->videoName = $videoName;
//
//        return $this;
//    }
//
//    /**
//     * Get videoName
//     *
//     * @return string 
//     */
//    public function getVideoName() {
//        return $this->videoName;
//    }

    //////////////////////////////
    ////// const n to str ///////
    //////////////////////////////
    
    public function __construct() {
        $this->sessions = new ArrayCollection();
        $this->images = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getNom();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Formation
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Formation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     * @return Formation
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string 
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set publicVise
     *
     * @param string $publicVise
     * @return Formation
     */
    public function setPublicVise($publicVise)
    {
        $this->publicVise = $publicVise;

        return $this;
    }

    /**
     * Get publicVise
     *
     * @return string 
     */
    public function getPublicVise()
    {
        return $this->publicVise;
    }

    /**
     * Set programme
     *
     * @param string $programme
     * @return Formation
     */
    public function setProgramme($programme)
    {
        $this->programme = $programme;

        return $this;
    }

    /**
     * Get programme
     *
     * @return string 
     */
    public function getProgramme()
    {
        return $this->programme;
    }

    /**
     * Set motDuDirecteur
     *
     * @param string $motDuDirecteur
     * @return Formation
     */
    public function setMotDuDirecteur($motDuDirecteur)
    {
        $this->motDuDirecteur = $motDuDirecteur;

        return $this;
    }

    /**
     * Get motDuDirecteur
     *
     * @return string 
     */
    public function getMotDuDirecteur()
    {
        return $this->motDuDirecteur;
    }

    /**
     * Set contactNom
     *
     * @param string $contactNom
     * @return Formation
     */
    public function setContactNom($contactNom)
    {
        $this->contactNom = $contactNom;

        return $this;
    }

    /**
     * Get contactNom
     *
     * @return string 
     */
    public function getContactNom()
    {
        return $this->contactNom;
    }

    /**
     * Set contactAdresse
     *
     * @param string $contactAdresse
     * @return Formation
     */
    public function setContactAdresse($contactAdresse)
    {
        $this->contactAdresse = $contactAdresse;

        return $this;
    }

    /**
     * Get contactAdresse
     *
     * @return string 
     */
    public function getContactAdresse()
    {
        return $this->contactAdresse;
    }

    /**
     * Set contactEmail
     *
     * @param string $contactEmail
     * @return Formation
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail
     *
     * @return string 
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Set contactTelephone
     *
     * @param string $contactTelephone
     * @return Formation
     */
    public function setContactTelephone($contactTelephone)
    {
        $this->contactTelephone = $contactTelephone;

        return $this;
    }

    /**
     * Get contactTelephone
     *
     * @return string 
     */
    public function getContactTelephone()
    {
        return $this->contactTelephone;
    }

    /**
     * Set contactSiteWeb
     *
     * @param string $contactSiteWeb
     * @return Formation
     */
    public function setContactSiteWeb($contactSiteWeb)
    {
        $this->contactSiteWeb = $contactSiteWeb;

        return $this;
    }

    /**
     * Get contactSiteWeb
     *
     * @return string 
     */
    public function getContactSiteWeb()
    {
        return $this->contactSiteWeb;
    }

    /**
     * Set videoLink
     *
     * @param string $videoLink
     * @return Formation
     */
    public function setVideoLink($videoLink)
    {
        $this->videoLink = $videoLink;

        return $this;
    }

    /**
     * Get videoLink
     *
     * @return string 
     */
    public function getVideoLink()
    {
        return $this->videoLink;
    }

    /**
     * Set duree
     *
     * @param string $duree
     * @return Formation
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return string 
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set cout
     *
     * @param string $cout
     * @return Formation
     */
    public function setCout($cout)
    {
        $this->cout = $cout;

        return $this;
    }

    /**
     * Get cout
     *
     * @return string 
     */
    public function getCout()
    {
        return $this->cout;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     * @return Formation
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Get activated
     *
     * @return boolean 
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     * @return Formation
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     * @return Formation
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime 
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set diplome
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome $diplome
     * @return Formation
     */
    public function setDiplome(\AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome $diplome = null)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome 
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Set etablissement
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Etablissement $etablissement
     * @return Formation
     */
    public function setEtablissement(\AfricaSchoolBus\Bundle\DataBundle\Entity\Etablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Etablissement 
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * Set modeEnseignement
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\ModeEnseignement $modeEnseignement
     * @return Formation
     */
    public function setModeEnseignement(\AfricaSchoolBus\Bundle\DataBundle\Entity\ModeEnseignement $modeEnseignement = null)
    {
        $this->modeEnseignement = $modeEnseignement;

        return $this;
    }

    /**
     * Get modeEnseignement
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\ModeEnseignement 
     */
    public function getModeEnseignement()
    {
        return $this->modeEnseignement;
    }

    /**
     * Set domaine
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine $domaine
     * @return Formation
     */
    public function setDomaine(\AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine $domaine = null)
    {
        $this->domaine = $domaine;

        return $this;
    }

    /**
     * Get domaine
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine 
     */
    public function getDomaine()
    {
        return $this->domaine;
    }

    /**
     * Set matiere
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matiere
     * @return Formation
     */
    public function setMatiere(\AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matiere = null)
    {
        $this->matiere = $matiere;

        return $this;
    }

    /**
     * Get matiere
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere 
     */
    public function getMatiere()
    {
        return $this->matiere;
    }

    /**
     * Add images
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\FormationImage $images
     * @return Formation
     */
    public function addImage(\AfricaSchoolBus\Bundle\DataBundle\Entity\FormationImage $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\FormationImage $images
     */
    public function removeImage(\AfricaSchoolBus\Bundle\DataBundle\Entity\FormationImage $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add sessions
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Session $sessions
     * @return Formation
     */
    public function addSession(\AfricaSchoolBus\Bundle\DataBundle\Entity\Session $sessions)
    {
        $this->sessions[] = $sessions;

        return $this;
    }

    /**
     * Remove sessions
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Session $sessions
     */
    public function removeSession(\AfricaSchoolBus\Bundle\DataBundle\Entity\Session $sessions)
    {
        $this->sessions->removeElement($sessions);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set video
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\FormationVideo $video
     * @return Formation
     */
    public function setVideo(\AfricaSchoolBus\Bundle\DataBundle\Entity\FormationVideo $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\FormationVideo 
     */
    public function getVideo()
    {
        return $this->video;
    }
}
