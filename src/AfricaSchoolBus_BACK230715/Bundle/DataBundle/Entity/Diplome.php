<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Diplome
 *
 * @ORM\Table(name="diplome")
 * @ORM\Entity
 */
class Diplome {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="diplome", type="string", length=255)
     */
    private $diplome;

    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="diplome")
     **/
    private $formations;

    public function __construct() {
        $this->formations = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getDiplome();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set diplome
     *
     * @param string $diplome
     * @return Diplome
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return string 
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return Diplome
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }
}
