<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Matiere
 *
 * @ORM\Table(name="matiere")
 * @ORM\Entity(repositoryClass="AfricaSchoolBus\Bundle\DataBundle\Entity\MatiereRepository")
 */
class Matiere {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="matiere", type="string", length=128)
     */
    private $matiere;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var decimal
     *
     * @ORM\Column(name="coefficient", type="decimal", options={"default" = 0})
     */
    private $coefficient;

    /**
     * @ORM\ManyToOne(targetEntity="Domaine", inversedBy="matieres")
     * @ORM\JoinColumn(name="domaine_id", referencedColumnName="id")
     * */
    private $domaine;
    
    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="matiere")
     **/
    private $formations;
    
    public function __construct() {
        $this->formations = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getMatiere();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set matiere
     *
     * @param string $matiere
     * @return Matiere
     */
    public function setMatiere($matiere) {
        $this->matiere = $matiere;

        return $this;
    }

    /**
     * Get matiere
     *
     * @return string 
     */
    public function getMatiere() {
        return $this->matiere;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Matiere
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set domaine
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine $domaine
     * @return Matiere
     */
    public function setDomaine(\AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine $domaine = null) {
        $this->domaine = $domaine;

        return $this;
    }

    /**
     * Get domaine
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine 
     */
    public function getDomaine() {
        return $this->domaine;
    }


    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return Matiere
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     * @return Matiere
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }
}
