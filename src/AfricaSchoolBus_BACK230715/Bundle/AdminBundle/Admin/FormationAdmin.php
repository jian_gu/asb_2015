<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;
use AfricaSchoolBus\Bundle\DataBundle\Form\Type\FormationImageType;
use AfricaSchoolBus\Bundle\DataBundle\Form\Type\FormationVideoType;
use AfricaSchoolBus\Bundle\DataBundle\Form\Type\SessionType;

class FormationAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Prérequis')
                ->add('domaine', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine',
                    'property' => 'domaine'
                ))
                ->add('matiere', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere',
                    'property' => 'matiere'))
                ->add('diplome', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome',
                    'property' => 'diplome',
                    'label' => 'Diplôle'))
                ->add('modeEnseignement', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\ModeEnseignement',
                    'property' => 'mode',
                    'label' => 'Mode d\'enseignement'))
                ->add('etablissement', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Etablissement',
                    'property' => 'etablissement',
                    'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('e')
                        ->where('e.activated = 1');
            },
                    'label' => 'Établissement',
                    'required' => FALSE))
                ->end()
                ->with('Détails')
                ->add('nom', 'text', array('label' => 'Nom de formation'))
                ->add('objectif', 'text', array('label' => 'Objectif pour cette formation'))
                ->add('publicVise', 'text', array('label' => 'Public visé'))
                ->add('programme', 'textarea', array('label' => 'Programme'))
                ->add('duree', 'text', array('label' => 'Durée'))
                ->add('cout', 'text', array('label' => 'Coût'))
                ->add('motDuDirecteur', 'textarea', array('label' => 'Mot du directeur'))
                ->add('contactNom', 'text', array('label' => 'Nom du contact'))
                ->add('contactAdresse', 'text', array('label' => 'Adresse du contact'))
                ->add('contactEmail', 'email', array('label' => 'Email du contact'))
                ->add('contactTelephone', 'text', array('label' => 'Téléphone du contact'))
                ->add('contactSiteWeb', 'url', array('label' => 'Lien site de la formation',
                    'required' => FALSE))
                ->add('sessions', 'collection', array(
                    'label' => 'Prochaines sessions',
                    'required' => FALSE,
                    'type' => new SessionType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'id',
                    'targetEntity' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Session',
                    'link_parameters' => array('context' => 'session'),
                        )
                )
                ->end()
                ->with('Supplémentaire')
                ->add('images', 'collection', array(
                    'label' => 'Image pour formation',
                    'required' => FALSE,
                    'type' => new FormationImageType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                        ), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'id',
                    'targetEntity' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\FormationImage',
                    'link_parameters' => array('context' => 'image'),
                        )
                )
                ->add('video', new FormationVideoType(), array(
                    'required' => FALSE
                ))

//                ->add('photoFile', 'vich_image', array(
//                    'label' => 'Photo de la formation',
//                    'required' => false,
//                    'allow_delete' => true, // not mandatory, default is true
//                    'download_link' => true, // not mandatory, default is true
//                    'attr' => array(
//                        'accept' => 'image/*',
//                        'multiple' => 'multiple'
//                    )
//                ))
//                ->add('videoFile', 'vich_file', array('label' => 'Video de la formation',
//                    'required' => false,
//                    'allow_delete' => true, // not mandatory, default is true
//                    'download_link' => true, // not mandatory, default is true
//                ))
                ->end()
                ->with('Activé')
                ->add('activated', null, array('label' => 'Activée',
                    'required' => false,))
                ->end()
                ->with('Coefficient')
                ->add('coefficient', 'integer', array('label' => 'Coefficient',
                    'required' => FALSE))
                ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('domaine')
                ->add('matiere')
                ->add('etablissement')
                ->add('nom')
                ->add('contactNom')
                ->add('contactTelephone')
                ->add('contactEmail')
                ->add('contactSiteWeb')
                ->add('activated', null, array(
                    'label' => 'Activée'
                ))
                ->add('coefficient')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('nom')
                ->add('domaine')
                ->add('matiere')
                ->add('activated', 'boolean', array(
                    'label' => 'Activée',
                    'editable' => TRUE
                ))
                ->add('coefficient')
        ;
    }

}
