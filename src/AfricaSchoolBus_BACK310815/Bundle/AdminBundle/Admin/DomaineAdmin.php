<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DomaineAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('domaine', 'text', array('label' => 'Domaine'))
                ->add('description', 'textarea', array('label' => 'Description',
                    'required' => FALSE))
                ->add('coefficient', 'integer', array('label' => 'Coefficient',
                    'required' => FALSE))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('domaine')
                ->add('coefficient')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('domaine')
                ->add('description')
                ->add('coefficient')
        ;
    }

}
