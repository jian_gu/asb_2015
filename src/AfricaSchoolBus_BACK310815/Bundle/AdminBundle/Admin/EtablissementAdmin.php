<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EtablissementAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Général')
                ->add('type', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\TypeEtablissement',
                    'property' => 'type',
                    'label' => 'Type d\'établisement'))
                ->add('etablissement', 'text', array('label' => 'Nom d\'établisement'))
                ->add('activated', null, array('label' => 'Activé',
                    'required' => false,))
                ->add('coefficient', 'integer', array('label' => 'Coefficient',
                    'required' => FALSE))
                ->end()
                ->with('Adresse')
                ->add('rue', 'text', array('label' => 'N° rue', 'required' => FALSE))
                ->add('cp', 'text', array('label' => 'CP', 'required' => FALSE))
                ->add('ville', 'text', array('label' => 'Ville'))
                ->add('pays', 'country', array('label' => 'Pays'))
                ->add('telephone', 'text', array('label' => 'Téléphone', 'required' => FALSE))
                ->add('siteWeb', 'text', array('label' => 'site web'))
                ->end()
                ->with('Logo')
                ->add('logoFile', 'vich_image', array('label' => 'Logo d\'établisement',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
                ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('type')
                ->add('etablissement')
                ->add('cp')
                ->add('ville')
                ->add('coefficient')
                ->add('activated', null, array(
                    'label' => 'Activée'
                ))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('etablissement')
                ->add('type')
                ->add('rue')
                ->add('cp')
                ->add('ville')
                ->add('activated', 'boolean', array(
                    'label' => 'Activée',
                    'editable' => TRUE
                ))
                ->add('coefficient')
        ;
    }

}
