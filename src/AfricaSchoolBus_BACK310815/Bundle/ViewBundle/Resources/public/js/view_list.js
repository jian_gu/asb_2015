/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var block3 = $('#div-list-block-3');
    var block3SrchForm = $('#div-list-block-3-contents-frm-srch');

    dynamicHeightBlock3SrchForm($(window).width(), $(block3).height());

    // Resize window, trigger function
    $(window).resize(function() {
        $(block3SrchForm).height(0);
        var windowWidth = $(window).width();
        var blockHeight = $(block3).height();
        dynamicHeightBlock3SrchForm(windowWidth, blockHeight);
    });

    /**
     * dynamicly resize search form height
     * @param {type} windowWidth
     * @param {type} blockHeight
     * @returns {undefined}
     */
    function dynamicHeightBlock3SrchForm(windowWidth, blockHeight) {
        if (windowWidth >= 768) {
            var newHeight = parseInt(blockHeight) + parseInt(90);
            $(block3SrchForm).height(newHeight);
        }
    }
});
