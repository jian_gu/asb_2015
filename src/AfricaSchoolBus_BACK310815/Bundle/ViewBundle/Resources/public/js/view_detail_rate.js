/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    ////////////////////////////
    //// Define variables //////
    ////////////////////////////
    var defaultStarWidth = "25px";
    var averageRateHolder = $('#comment-average-rate');
    var averageRate = parseFloat($('#comment-average-rate').attr('data'));
    var averageRateStarWith = $('#comment-average-rate').attr('width') + 'px';
    var userRateHolder = $('.comment-user-rate');
    var userRateStarWidth = $('.comment-user-rate').attr('width') + 'px';
    var rateSelect = $('select.asb-detail-comment-rate-select');
    var userSetRateHolder = $('.asb-detail-comment-rate-star');
    var userSetRateHolderWidth = $('.asb-detail-comment-rate-star').attr('width') + 'px';

    ////////////////////////////
    ///// Call functions //////
    ////////////////////////////

    // Average rate
    showEverageRate($(averageRateHolder), averageRate, averageRateStarWith);
    // Single user rate
    $(userRateHolder).each(function(index, element) {
        var userRate = parseInt($(element).attr('data'));
        showSingleUserRate($(element), userRate, userRateStarWidth);
    });
    // Construct user rate star
    constructRateForForm($(userSetRateHolder), userSetRateHolderWidth);

    ////////////////////////////
    /////// My functions ///////
    ////////////////////////////
    /**
     * Show everage rating value, decimal value allowed
     * @param {dom element} starHolder
     * @param {integer} rateNum
     * @param {integer} starWidth
     * @returns {undefined}
     */
    function showEverageRate(starHolder, rateNum, starWidth) {
        $(starHolder).rateYo({
            starWidth: starWidth,
            rating: rateNum,
            fullStar: false,
            readOnly: true
        });
    }

    /**
     * Show rated stars, related to user rate value
     * @param {dom element} starHolder
     * @param {integer} rateNum
     * @param {integer} starWidth
     * @returns {undefined}
     */
    function showSingleUserRate(starHolder, rateNum, starWidth) {
        $(starHolder).rateYo({
            starWidth: starWidth,
            rating: rateNum,
            fullStar: true,
            readOnly: true
        });
    }

    /**
     * Construct rating star, set default value to 3/5
     * @param {dom element} starHolder
     * @param {integer} starWidth
     * @returns {undefined}
     */
    function constructRateForForm(starHolder, starWidth) {
        $(starHolder).rateYo({
            starWidth: starWidth,
            rating: 3,
            fullStar: true,
            onChange: function(rating, rateYoInstance) {
                if(rating < 1){
                    $(this).rateYo("rating", 1);
                }
                $(this).next().text(rating);
            },
            onSet: function(rating, rateYoInstance) {
                var validRating = rating;
                if(rating < 1){
                    $(this).rateYo("rating", 1);
                    validRating = 1;
                }
                /**
                 * Set select value, as same as the rate star value
                 */
                $(rateSelect).val(validRating);
            }
        });
    }
});