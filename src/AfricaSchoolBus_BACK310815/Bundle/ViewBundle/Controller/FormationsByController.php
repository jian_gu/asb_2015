<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FormationsByController extends Controller {

    static $codeCountArray = array();

    /**
     * Action formation by country
     * @return type
     */
    public function FormationByCountryAction() {
        // Generate country code : count array
        $this->_generateCodeCountsArray();
        return $this->render('ASBViewBundle:FormationsBy:Country.html.twig', array(
                    'countryCount' => self::$codeCountArray,
        ));
    }

    ////////////////////////////////
    ////////////////////////////////
    /**
     * Get all countries african francophone
     * @return object
     */
    private function _getPaysAfricanFrancophone() {
        $em = $this->getDoctrine()->getManager();
        $pays = $em->getRepository("ASBDataBundle:PaysFranco")->findAll();
        return $pays ? $pays : NULL;
    }

    /**
     * Generate country:count pairs, and assign them to static var
     */
    private function _generateCodeCountsArray() {
        // Get all pays african francophone
        $codeArray = $this->_getPaysAfricanFrancophone();
        foreach ($codeArray as $codeObj) {
            $code = $codeObj->getCountryCode();
            $count = $this->_getCountFormationByCountryCode($code);
            if ($count != NULL) {
                self::$codeCountArray[$code] = $count;
            }
        }
    }

    /**
     * Get formation count by country code
     * @param string $code
     * @return string
     */
    private function _getCountFormationByCountryCode($code) {
        $em = $this->getDoctrine()->getManager();
        $count = $em->getRepository("ASBDataBundle:Formation")->getCountFormationByCountry($code);
        return $count && count($count) >= 1 ? current($count) : NULL;
    }

}
