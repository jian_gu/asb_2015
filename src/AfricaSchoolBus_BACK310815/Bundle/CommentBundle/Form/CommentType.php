<?php

namespace AfricaSchoolBus\Bundle\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommentType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('formation', null, array(
                    'attr' => array('hidden' => 'hidden')
                    ))
                ->add('comment')
                ->add('rate', 'choice', array(
                    'choices' => array(
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                        5 => 5,
                    ),
                    'data' => 3,
                    'attr' => array('hidden' => 'hidden')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AfricaSchoolBus\Bundle\CommentBundle\Entity\Comment'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'africaschoolbus_bundle_commentbundle_comment';
    }

}
