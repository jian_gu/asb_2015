<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Domaine
 *
 * @ORM\Table(name="domaine")
 * @ORM\Entity
 */
class Domaine {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="domaine", type="string", length=128)
     */
    private $domaine;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var decimal
     *
     * @ORM\Column(name="coefficient", type="decimal", options={"default" = 0})
     */
    private $coefficient;

    /**
     * @ORM\OneToMany(targetEntity="Matiere", mappedBy="domaine")
     * */
    private $matieres;
    
    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="domaine")
     **/
    private $formations;
    
    /**
     * Constructor
     */
    public function __construct() {
        $this->matieres = new ArrayCollection();
        $this->formations = new ArrayCollection();
    }

    public function __toString() {
        return (string) $this->getDomaine();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set domaine
     *
     * @param string $domaine
     * @return Domaine
     */
    public function setDomaine($domaine) {
        $this->domaine = $domaine;

        return $this;
    }

    /**
     * Get domaine
     *
     * @return string 
     */
    public function getDomaine() {
        return $this->domaine;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Domaine
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Add matieres
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matieres
     * @return Domaine
     */
    public function addMatiere(\AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matieres) {
        $this->matieres[] = $matieres;

        return $this;
    }

    /**
     * Remove matieres
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matieres
     */
    public function removeMatiere(\AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere $matieres) {
        $this->matieres->removeElement($matieres);
    }

    /**
     * Get matieres
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMatieres() {
        return $this->matieres;
    }


    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return Domaine
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     * @return Domaine
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }
}
