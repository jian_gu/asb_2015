<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Publicite
 *
 * @ORM\Table(name="publicite")
 * @ORM\Entity()
 * @Vich\Uploadable
 */
class Publicite {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sponsorise_par", type="string", length=128)
     */
    private $sponsoriseePar;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=128)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="lien", type="string", length=255)
     */
    private $lien;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", options={"default" = 1})
     */
    private $activated;

    /**
     * @var decimal
     *
     * @ORM\Column(name="coefficient", type="decimal", options={"default" = 0})
     */
    private $coefficient;
    
    ///////////// logo //////////////
    /////////////////////////////////
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="publicite_logo", fileNameProperty="logoName")
     * 
     * @var File $logoFile
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255, name="logo_name", nullable=true)
     *
     * @var string $logoName
     */
    private $logoName;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $logo
     */
    public function setLogoFile(File $logo = null) {
        $this->logoFile = $logo;

        if ($logo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        $this->setUpdateAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getLogoFile() {
        return $this->logoFile;
    }

    /**
     * Set logoName
     *
     * @param string $logoName
     * @return Etablissement
     */
    public function setLogoName($logoName) {
        $this->logoName = $logoName;

        return $this;
    }

    /**
     * Get logoName
     *
     * @return string 
     */
    public function getLogoName() {
        return $this->logoName;
    }
    
    //////////// image //////////////
    /////////////////////////////////
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="publicite_image", fileNameProperty="imageName")
     * 
     * @var File $imageFile
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, name="image_name", nullable=true)
     *
     * @var string $imageName
     */
    private $imageName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null) {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        $this->setUpdateAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getImageFile() {
        return $this->imageFile;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     * @return Publicite
     */
    public function setImageName($imageName) {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string 
     */
    public function getImageName() {
        return $this->imageName;
    }
    
    //////////// video //////////////
    /////////////////////////////////
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="publicite_video", fileNameProperty="videoName")
     * 
     * @var File $videoFile
     */
    private $videoFile;

    /**
     * @ORM\Column(type="string", length=255, name="video_name", nullable=true)
     *
     * @var string $videoName
     */
    private $videoName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $video
     */
    public function setVideoFile(File $video = null) {
        $this->videoFile = $video;

        if ($video) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        $this->setUpdateAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getVideoFile() {
        return $this->videoFile;
    }

    /**
     * Set videoName
     *
     * @param string $videoName
     * @return Publicite
     */
    public function setVideoName($videoName) {
        $this->videoName = $videoName;

        return $this;
    }

    /**
     * Get videoName
     *
     * @return string 
     */
    public function getVideoName() {
        return $this->videoName;
    }
    
    public function __construct() {
        
    }
    
    public function __toString() {
        return (string) $this->getTitre();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sponsorisePar
     *
     * @param string $sponsorisePar
     * @return Publicite
     */
    public function setSponsorisePar($sponsorisePar)
    {
        $this->sponsorisePar = $sponsorisePar;

        return $this;
    }

    /**
     * Get sponsorisePar
     *
     * @return string 
     */
    public function getSponsorisePar()
    {
        return $this->sponsorisePar;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Publicite
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Publicite
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lien
     *
     * @param string $lien
     * @return Publicite
     */
    public function setLien($lien)
    {
        $this->lien = $lien;

        return $this;
    }

    /**
     * Get lien
     *
     * @return string 
     */
    public function getLien()
    {
        return $this->lien;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     * @return Publicite
     */
    public function setUpdateAt($updateAt)
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime 
     */
    public function getUpdateAt()
    {
        return $this->updateAt;
    }

    /**
     * Set blocked
     *
     * @param boolean $blocked
     * @return Publicite
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * Get blocked
     *
     * @return boolean 
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * Set sponsoriseePar
     *
     * @param string $sponsoriseePar
     * @return Publicite
     */
    public function setSponsoriseePar($sponsoriseePar)
    {
        $this->sponsoriseePar = $sponsoriseePar;

        return $this;
    }

    /**
     * Get sponsoriseePar
     *
     * @return string 
     */
    public function getSponsoriseePar()
    {
        return $this->sponsoriseePar;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     * @return Publicite
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Get activated
     *
     * @return boolean 
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     * @return Publicite
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }
}
