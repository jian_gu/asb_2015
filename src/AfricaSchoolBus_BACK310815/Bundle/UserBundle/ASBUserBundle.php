<?php

namespace AfricaSchoolBus\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ASBUserBundle extends Bundle
{
    public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
