<?php

namespace AfricaSchoolBus\Bundle\CommentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AfricaSchoolBus\Bundle\CommentBundle\Entity\Comment;
use AfricaSchoolBus\Bundle\DataBundle\Entity\Formation;
use AfricaSchoolBus\Bundle\CommentBundle\Form\CommentType;

/**
 * Comment controller.
 *
 */
class CommentController extends Controller {

    /**
     * Lists all Comment entities.
     *
     */
    public function indexAction(Formation $formation) {
        $idFormation = $formation->getId();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ASBCommentBundle:Comment')->getCommentsByFormaiton($idFormation);

        return $this->render('ASBCommentBundle:Comment:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Comment entity.
     *
     */
    public function createAction(Request $request) {
        $formationNom = $request->query->get('formation_title');
        $formationId = $request->query->get('formation_id');
        $em = $this->getDoctrine()->getManager();
        $formation = $em->getRepository('ASBDataBundle:Formation')->findOneById($formationId);

        $entity = new Comment();
        $form = $this->createCreateForm($entity, $formation);
        $form->handleRequest($request);

        if ($form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
            $user = $this->getConnectedUser();
            $entity->setUser($user);
            $entity->setActive(TRUE);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('africaschoolbus_view_detail_index', array('formation_title' => $formationNom, 'formation_id' => $formationId)));
        }

        return $this->render('ASBCommentBundle:Comment:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Comment $entity, Formation $formation = NULL) {
        $form = $this->createForm(new CommentType(), $entity, array(
            'action' => $this->generateUrl('comment_create', array('formation_title' => $formation->getNom(), 'formation_id' => $formation->getId())),
            'method' => 'POST',
        ));

        $form->get('formation')->setData($formation);

        $form->add('submit', 'submit', array('label' => 'Soumettre'));

        return $form;
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     */
    public function newAction(Formation $formation = NULL) {
        $entity = new Comment();
        $form = $this->createCreateForm($entity, $formation);

        return $this->render('ASBCommentBundle:Comment:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Comment entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ASBCommentBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ASBCommentBundle:Comment:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Comment entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ASBCommentBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ASBCommentBundle:Comment:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Comment $entity) {
        $form = $this->createForm(new CommentType(), $entity, array(
            'action' => $this->generateUrl('comment_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Comment entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ASBCommentBundle:Comment')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Comment entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('comment_edit', array('id' => $id)));
        }

        return $this->render('ASBCommentBundle:Comment:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Comment entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ASBCommentBundle:Comment')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Comment entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('comment'));
    }

    /**
     * Creates a form to delete a Comment entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('comment_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    /**
     * Get average rate for one particular formation
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formation
     * @return type
     */
    public function averageRateAction(Formation $formation) {
        $idFormation = $formation->getId();

        $em = $this->getDoctrine()->getManager();

        $averageRate = $em->getRepository('ASBCommentBundle:Comment')->getAverageRate($idFormation);

        return $this->render('ASBCommentBundle:Comment:averageRate.html.twig', array(
                    'averageRate' => $averageRate,
        ));
    }
    
    /**
     * get logged user
     * @return null
     */
    private function getConnectedUser(){
        $user = $this->getUser();
        if(!$user || $user == NULL){
            return NULL;
        }
        return $user;
    }

}
