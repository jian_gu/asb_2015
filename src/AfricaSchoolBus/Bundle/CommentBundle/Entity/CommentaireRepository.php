<?php

namespace AfricaSchoolBus\Bundle\CommentBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CommentaireRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CommentaireRepository extends EntityRepository {

    public function getCommentsByFormaiton($idFormation){
        $em = $this->getEntityManager();
        $parameters = array('idFormaiton' => $idFormation);
        $query = $em->createQuery(
                "SELECT c "
                . "FROM ASBCommentBundle:Comment c "
                . "JOIN c.formation f "
                . "WHERE c.active = 1 "
                . "AND f.id = :idFormaiton "
                . "ORDER BY c.dateTime DESC"
        );
        $query->setParameters($parameters);
        $result = $query->getResult();
        return $result;
    } 
    
    /**
     * Get average rating for a particular formation
     * @param type $idFormation
     * @return type
     */
    public function getAverageRate($idFormation) {
        $em = $this->getEntityManager();
        $parameters = array('idFormaiton' => $idFormation);
        $query = $em->createQuery(
                "SELECT AVG(c.rate) "
                . "FROM ASBCommentBundle:Comment c "
                . "JOIN c.formation f "
                . "WHERE c.active = 1 "
                . "AND f.id = :idFormaiton "
                . "ORDER BY c.dateTime DESC"
        );
        $query->setParameters($parameters);
        $result = $query->getSingleScalarResult();
        return $result;
    }

}
