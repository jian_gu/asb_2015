<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ListController extends Controller {

    public function IndexAction(Request $request) {
        // Create keyword search form
        $keywordSrchForm = $this->createKeywordSearchForm();
        // Create criterias search form
        $criteriasSrchForm = $this->createCriteriasSearchForm();
        // Get request get parameter array
        $parameters = $this->getUrlParameters($request);
        // Get page current number
        $page = $request->query->getInt('page', 1);
        // Get results
        $pagination = $this->searchFormationsByMappedFileds($parameters, $page);
        // Get publicites
        $pubs = $this->getPubsCoefficient2();

        // Pagination

        return $this->render('ASBViewBundle:List:Index.html.twig', array(
                    'keywordSrchForm' => $keywordSrchForm->createView(),
                    'criteriasSrchForm' => $criteriasSrchForm->createView(),
                    'pagination' => $pagination,
                    'publicites' => count($pubs) > 0 ? $pubs : NULL,
        ));
    }

    private function createKeywordSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $textBtnSearch = $this->get('translator')->trans('srch_form.button.search');
        $form = $this->get('form.factory')->createNamedBuilder('frm_srch_keyword', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('POST')
                ->add('keyword', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                ))
                ->add('search', 'submit', array(
                    'label' => $textBtnSearch,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

    private function createCriteriasSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $textBtnFilter = $this->get('translator')->trans('srch_form.button.filter');
        $textSelctCountry = $this->get('translator')->trans('srch_form.select.country');
        $textSelctDomain = $this->get('translator')->trans('srch_form.select.domain');
        $textSelctDegree = $this->get('translator')->trans('srch_form.select.degree');
        $textSelctMode = $this->get('translator')->trans('srch_form.select.mode');
        $form = $this->get('form.factory')->createNamedBuilder('frm_srch_criterias', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('GET')
                ->add('pays', 'country', array(
                    'label' => 'Pays',
//                    'constraints' => array(),
                    'required' => FALSE,
                    'placeholder' => $textSelctCountry,
                    'empty_data' => null
                ))
                ->add('domaine', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine',
                    'property' => 'domaine',
                    'label' => 'Domaine',
                    'constraints' => array(),
                    'required' => FALSE,
                    'placeholder' => $textSelctDomain,
                    'empty_data' => null
                ))
                ->add('diplome', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome',
                    'property' => 'diplome',
                    'label' => 'Diplôme',
                    'constraints' => array(),
                    'required' => FALSE,
                    'placeholder' => $textSelctDegree,
                    'empty_data' => null
                ))
                ->add('mode', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\ModeEnseignement',
                    'property' => 'mode',
                    'label' => 'Mode',
                    'constraints' => array(),
                    'required' => FALSE,
                    'placeholder' => $textSelctMode,
                    'empty_data' => null
                ))
//                ->add('matiere', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere',
//                    'property' => 'matiere',
//                    'label' => 'Matière',
//                    'constraints' => array(),
//                    'required' => FALSE,
//                ))
                ->add('search', 'submit', array(
                    'label' => $textBtnFilter,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

    public function SearchFormationsByCriteriasAction(Request $request) {
        $parameters = $this->getUrlParameters($request);
        // Get page current number
        $page = $request->query->getInt('page', 1);
        $pagination = $this->searchFormationsByMappedFileds($parameters, $page);
        return $this->render('ASBViewBundle:List:Index.html.twig', array(
                    'pagination' => $pagination,
        ));
    }

    private function getUrlParameters($request) {
        // Get all request get query parameters
        $getParameters = $request->query->all();
        // Declare returned array
        $parameters = array();
        array_key_exists('keyword', $getParameters) ? $parameters['keyword'] = $getParameters['keyword'] : NULL;
        array_key_exists('pays', $getParameters) ? $parameters['pays'] = strtolower($getParameters['pays']) : NULL;
        array_key_exists('domaine', $getParameters) ? $parameters['domaine'] = $getParameters['domaine'] : NULL;
        array_key_exists('diplome', $getParameters) ? $parameters['diplome'] = $getParameters['diplome'] : NULL;
        array_key_exists('mode', $getParameters) ? $parameters['mode'] = $getParameters['mode'] : NULL;
        return $parameters;
    }

    private function searchFormationsByMappedFileds($parameters, $page = 1) {
        $finder = $this->container->get('fos_elastica.finder.asb_search.formation');
        $query = new \Elastica\Query\Bool();

        $activatedQuery = new \Elastica\Query\Term();
        $activatedQuery->setTerm('activated', 1);
        $query->addShould($activatedQuery);

        if (empty($parameters)) {
            $query = new \Elastica\Query\MatchAll();
        } else {

            // Keyword
            if (array_key_exists('keyword', $parameters)) {
                $keywordQuery = new \Elastica\Query\QueryString();
                $keywordQuery->setQuery('*' . $parameters['keyword'] . '*');
                $query->addMust($keywordQuery);
            }

            // Pays
            if (array_key_exists('pays', $parameters)) {
                $nestedPaysQuery = new \Elastica\Query\Nested();
                $paysQuery = new \Elastica\Query\Term(array('pays' => array('value' => $parameters['pays'])));
                $nestedPaysQuery->setQuery($paysQuery);
                $nestedPaysQuery->setPath('etablissement');
                $query->addMust($nestedPaysQuery);
            }

            // Domaine
            if (array_key_exists('domaine', $parameters)) {
                $nestedDomaineFilter = new \Elastica\Query\Nested();
                $domaineQuery = new \Elastica\Query\Term(array('domaine.id' => array('value' => $parameters['domaine'])));
                $nestedDomaineFilter->setQuery($domaineQuery);
                $nestedDomaineFilter->setPath('domaine');
                $query->addMust($nestedDomaineFilter);
            }

            // Diplome
            if (array_key_exists('diplome', $parameters)) {
                $nestedDiplomeQuery = new \Elastica\Query\Nested();
                $diplomeQuery = new \Elastica\Query\Term(array('diplome.id' => array('value' => $parameters['diplome'])));
                $nestedDiplomeQuery->setQuery($diplomeQuery);
                $nestedDiplomeQuery->setPath('diplome');
                $query->addMust($nestedDiplomeQuery);
            }

            // Mode d'enseignement
            if (array_key_exists('mode', $parameters)) {
                $nestedModeQuery = new \Elastica\Query\Nested();
                $modeQuery = new \Elastica\Query\Term(array('modeEnseignement.id' => array('value' => $parameters['mode'])));
                $nestedModeQuery->setQuery($modeQuery);
                $nestedModeQuery->setPath('modeEnseignement');
                $query->addMust($nestedModeQuery);
            }
        }

        // Option 1. Returns all users who have example.net in any of their mapped fields
//        $results = $finder->find('$index');
        // Option 2. Returns a set of hybrid results that contain all Elasticsearch results
        // and their transformed counterparts. Each result is an instance of a HybridResult
//        $results = $finder->findHybrid('$index');
        // Option 3b. KnpPaginator resultset
        $paginator = $this->get('knp_paginator');
        $results = $finder->find($query);
//        $test = array($results[0], $results[0], $results[0], $results[0], $results[0], $results[0], $results[0],$results[0], $results[0], $results[0], $results[0], $results[0], $results[0], $results[0]);
        // Get parameter for number items per page
        $itemPerPage = $this->container->getParameter('asb_list_items_per_page');
        $pagination = $paginator->paginate($results, $page, $itemPerPage);
        return $pagination;
    }

    /**
     * Get publicites
     * @return type
     */
    private function getPubsCoefficient2() {
        $conditions = array('coefficient' => 2, 'activated' => 1);
        $publicites = $this->getDoctrine()
                ->getRepository('ASBDataBundle:Publicite')
                ->findBy($conditions);
        return $publicites;
    }

}
