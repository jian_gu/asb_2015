<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class HomeController extends Controller {

    public function indexAction() {
        // Create keyword search form
        $keywordSrchForm = $this->createKeywordSearchForm();
        // Formation to be shown at home page
        $frontFormation = $this->findZoomSurUneFormation();
        // Create criterias search form
//        $criteriasSrchForm = $this->createCriteriasSearchForm();
        return $this->render('ASBViewBundle:Home:index.html.twig', array(
                    'name' => "Jian",
                    'keywordSrchForm' => $keywordSrchForm->createView(),
                    'frontFormation' => $frontFormation,
//                    'criteriasSrchForm' => $criteriasSrchForm->createView(),
        ));
    }

    private function createKeywordSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $textBtnSearch = $this->get('translator')->trans('srch_form.button.search');
        $form = $this->get('form.factory')->createNamedBuilder('frm_srch_keyword', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('POST')
                ->add('keyword', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                ))
                ->add('search', 'submit', array(
                    'label' => $textBtnSearch,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

    /**
     * Find formation to be shown at home page, limit set to one by default
     * @param type $limit
     * @return type
     */
    private function findFormationsOrderByCoefficient($limit = 1) {
        $formation = $this->getDoctrine()
                ->getRepository('ASBDataBundle:Formation')
                ->findFormationsOrderByCoefficient($limit);
        return $formation != NULL && count($formation) > 0 ? $formation[0] : NULL;
    }

    /**
     * Find on "zoom sur une" formation
     * @return type
     */
    private function findZoomSurUneFormation() {
        $conditions = array("activated" => 1);
        $formation = $this->getDoctrine()
                ->getRepository('ASBDataBundle:ZoomSurFormation')
                ->findOneBy($conditions);
        return $formation != NULL ? $formation : NULL;
    }

    private function createCriteriasSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $form = $this->get('form.factory')->createNamedBuilder('frm_srch_criterias', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('GET')
                ->add('domaine', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine',
                    'property' => 'domaine',
                    'label' => 'Domaine',
                    'constraints' => array(),
                    'required' => FALSE,
                ))
                ->add('matiere', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Matiere',
                    'property' => 'matiere',
                    'label' => 'Matière',
                    'constraints' => array(),
                    'required' => FALSE,
                ))
                ->add('diplome', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Diplome',
                    'property' => 'diplome',
                    'label' => 'Diplôme',
                    'constraints' => array(),
                    'required' => FALSE,
                ))
                ->add('search', 'submit', array(
                    'label' => 'Recherche',
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

}
