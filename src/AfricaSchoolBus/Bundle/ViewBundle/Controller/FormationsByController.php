<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class FormationsByController extends Controller {

    static $codeCountArray = array();

    /**
     * Action formation by country
     * @return type
     */
    public function FormationByCountryAction() {
        // Create keyword search form
        $keywordSrchForm = $this->_createKeywordSearchForm();
        // Generate country code : count array
        $this->_generateCodeCountsArray();
        return $this->render('ASBViewBundle:FormationsBy:Country.html.twig', array(
                    'keywordSrchForm' => $keywordSrchForm->createView(),
                    'countryCount' => self::$codeCountArray,
        ));
    }

    ////////////////////////////////
    ////////////////////////////////
    /**
     * Get all countries african francophone
     * @return object
     */
    private function _getPaysAfricanFrancophone() {
        $em = $this->getDoctrine()->getManager();
        $pays = $em->getRepository("ASBDataBundle:PaysFranco")->findAll();
        return $pays ? $pays : NULL;
    }

    /**
     * Generate country:count pairs, and assign them to static var
     */
    private function _generateCodeCountsArray() {
        // Get all pays african francophone
        $codeArray = $this->_getPaysAfricanFrancophone();
        foreach ($codeArray as $codeObj) {
            $code = $codeObj->getCountryCode();
            $count = $this->_getCountFormationByCountryCode($code);
            if ($count != NULL) {
                self::$codeCountArray[$code] = $count;
            }
        }
    }

    /**
     * Get formation count by country code
     * @param string $code
     * @return string
     */
    private function _getCountFormationByCountryCode($code) {
        $em = $this->getDoctrine()->getManager();
        $count = $em->getRepository("ASBDataBundle:Formation")->getCountFormationByCountry($code);
        return $count && count($count) >= 1 ? current($count) : NULL;
    }

    private function _createKeywordSearchForm() {
        $defaultData = array('message' => 'Type your message here');
        $textBtnSearch = $this->get('translator')->trans('srch_form.button.search');
        $form = $this->get('form.factory')->createNamedBuilder('frm_srch_keyword', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_list_srch_criterias'))
                ->setMethod('POST')
                ->add('keyword', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                ))
                ->add('search', 'submit', array(
                    'label' => $textBtnSearch,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

}
