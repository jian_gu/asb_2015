<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactController extends Controller {

    public function indexAction() {
        return $this->render('ASBViewBundle:Contact:index.html.twig', array(
                        // ...
        ));
    }

    public function EmailAction() {
        // Get contact form
        $contactForm = $this->createContactForm();
        return $this->render('ASBViewBundle:Contact:Email.html.twig', array(
                    'contactForm' => $contactForm->createView(),
        ));
    }

    public function EmailProcessAction(Request $request) {
        $request = $this->container->get('request');
        $nom = $request->request->get('nom');
        $prenom = $request->request->get('prenom');
        $email = $request->request->get('email');
        $object = $request->request->get('object');
        $message = $request->request->get('message');
        $context = array('object' => $object, 'nom' => $nom, 'prenom' => $prenom, 'email' => $email, 'message' => $message);
        // Get google captcha post data
        $captcha = $request->request->get('g_recaptcha_response');
        $captchaValidated = $this->_checkGoogleCaptcha($captcha);

        if ($captchaValidated) {
            $mailer = $this->container->get("asb.mailer");
            $noreply = $mailer->parameters['address']['noreply'];
            $toAdmin = $mailer->parameters['address']['contact']['contact'];
            $objectDisplay = "Demande de contact";
            // sending process start
            $mailer->sendMail("contactToUser", $noreply, $email, $objectDisplay, $context);
            $mailer->sendMail("contactToAdmin", $email, $toAdmin, $objectDisplay, $context);
            // sending process end

            $results = new \stdClass();
            $results->success = TRUE;
            $results->error = FALSE;
            $results->msg = "sent";
        } else {
            $results = new \stdClass();
            $results->success = FALSE;
            $results->error = TRUE;
            $results->msg = "no-captcha";
        }

        $resultsJsonFy = json_encode($results);

        $resultBoundsUtf8Fy = utf8_encode($resultsJsonFy);
        return new Response($resultBoundsUtf8Fy);
    }

    /**
     * 
     * @return type
     */
    private function createContactForm() {
        $defaultData = array('message' => '');
        // Translations
        $textNom = $this->get('translator')->trans('contact_form.title.last_name');
        $textPrenom = $this->get('translator')->trans('contact_form.title.first_name');
        $textEmail = $this->get('translator')->trans('contact_form.title.email_address');
        $textObjet = $this->get('translator')->trans('contact_form.title.object');
        $textMessage = $this->get('translator')->trans('contact_form.title.message');
        $textButton = $this->get('translator')->trans('contact_form.button.send');

        $form = $this->get('form.factory')->createNamedBuilder('frm_contact_email', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_contact_email_process'))
                ->setMethod('POST')
                ->add('nom', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                    'label' => $textNom,
                    'required' => FALSE,
                ))
                ->add('prenom', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                    'label' => $textPrenom,
                    'required' => FALSE,
                ))
                ->add('email', 'email', array('constraints' => array(
                        new NotBlank(),
                    ),
                    'label' => $textEmail,
                ))
                ->add('object', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                    'label' => $textObjet,
                ))
                ->add('message', 'textarea', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 5)),
                    ),
                    'label' => $textMessage,
                ))
                ->add('send', 'submit', array(
                    'label' => $textButton,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

    /**
     * Check captcha / Bot
     * @param type $captcha
     * @return boolean
     * @author Jian GU <g.jian@almteam-consulting.com>
     */
    private function _checkGoogleCaptcha($captcha) {
        $responseStr = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lc8iwcTAAAAAM93RQNL64D8pTBjYVCQ7bcESJib&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $response = json_decode($responseStr);

        if ($response->success == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
