<?php

namespace AfricaSchoolBus\Bundle\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AfricaSchoolBus\Bundle\DataBundle\Entity\Newsletter;

class NewsletterController extends Controller {

    public function indexAction() {
        // Get subscription form
        $subscription = $this->createSubscrptForm();
        return $this->render('ASBViewBundle:Newsletter:index.html.twig', array(
                    'subscrptForm' => $subscription->createView(),
        ));
    }

    public function processAction(Request $request) {
        $request = $this->container->get('request');
        $nom = $request->request->get('nom');
        $prenom = $request->request->get('prenom');
        $email = trim($request->request->get('email'));
        $context = array('object' => "Abonnement à newsletter", 'nom' => $nom, 'prenom' => $prenom, 'email' => $email);
        // Get google captcha post data
        $captcha = $request->request->get('g_recaptcha_response');
        $captchaValidated = $this->_checkGoogleCaptcha($captcha);
        // Test existance of the email address
        $emailExists = $this->_checkEmailExistance($email);

        if ($captchaValidated) {
            if ($emailExists) {
                $results = new \stdClass();
                $results->success = FALSE;
                $results->error = TRUE;
                $results->msg = "email-exists";
            } else {
                // Save subscription into DB
                $this->_saveSubscription($nom, $prenom, $email);
                $mailer = $this->container->get("asb.mailer");
                $noreply = $mailer->parameters['address']['noreply'];
                $toAdmin = $mailer->parameters['address']['newsletter']['admin'];
                $objectDisplay = "Abonnement à newsletter -- AfricaSchoolBus";
                // sending process start
                $mailer->sendMail("newsletterToUser", $noreply, $email, $objectDisplay, $context);
                $mailer->sendMail("newsletterToAdmin", $email, $toAdmin, $objectDisplay, $context);
                // sending process end

                $results = new \stdClass();
                $results->success = TRUE;
                $results->error = FALSE;
                $results->msg = "sent";
            }
        } else {
            $results = new \stdClass();
            $results->success = FALSE;
            $results->error = TRUE;
            $results->msg = "no-captcha";
        }

        $resultsJsonFy = json_encode($results);

        $resultBoundsUtf8Fy = utf8_encode($resultsJsonFy);
        return new Response($resultBoundsUtf8Fy);
    }

    /**
     * 
     * @return type
     */
    private function createSubscrptForm() {
        $defaultData = array('message' => '');
        // Translations
        $textNom = $this->get('translator')->trans('subscrp_form.title.last_name');
        $textPrenom = $this->get('translator')->trans('subscrp_form.title.first_name');
        $textEmail = $this->get('translator')->trans('subscrp_form.title.email_address');
        $textButton = $this->get('translator')->trans('subscrp_form.button.send');

        $form = $this->get('form.factory')->createNamedBuilder('frm_subscription_newsletter', 'form', $defaultData, array())
//                $this->createFormBuilder($defaultData)
                ->setAction($this->generateUrl('africaschoolbus_view_newsletter_process'))
                ->setMethod('POST')
                ->add('nom', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                    'label' => $textNom,
                    'required' => FALSE,
                ))
                ->add('prenom', 'text', array('constraints' => array(
                        new NotBlank(),
                        new Length(array('min' => 2)),
                    ),
                    'label' => $textPrenom,
                    'required' => FALSE,
                ))
                ->add('email', 'email', array('constraints' => array(
                        new NotBlank(),
                    ),
                    'label' => $textEmail,
                ))
                ->add('send', 'submit', array(
                    'label' => $textButton,
                    'attr' => array(
                        'readonly' => 'readonly'
                    )
                ))
                ->getForm();
        return $form;
    }

    /**
     * Check captcha / Bot
     * @param type $captcha
     * @return boolean
     * @author Jian GU <g.jian@almteam-consulting.com>
     */
    private function _checkGoogleCaptcha($captcha) {
        $responseStr = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lc8iwcTAAAAAM93RQNL64D8pTBjYVCQ7bcESJib&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $response = json_decode($responseStr);

        if ($response->success == false) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Check if un abonnement with an adress email is already exist
     * @param type $email
     * @return boolean
     */
    private function _checkEmailExistance($email) {
        $condition = array('email' => $email);
        $em = $this->getDoctrine()->getManager();
        $abonnement = $em->getRepository('ASBDataBundle:Newsletter')->findBy($condition);
        if ($abonnement != NULL && count($abonnement) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     * Save subscription into db
     * @param type $nom
     * @param type $prenom
     * @param type $email
     */
    private function _saveSubscription($nom, $prenom, $email) {
        $entity = new Newsletter();
        $entity->setNom($nom);
        $entity->setPrenom($prenom);
        $entity->setEmail($email);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $em->clear();
    }

}
