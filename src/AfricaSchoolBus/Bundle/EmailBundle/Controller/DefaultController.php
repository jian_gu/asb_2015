<?php

namespace AfricaSchoolBus\Bundle\EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller {

    protected $mailer;
    protected $router;
    protected $twig;
    public $parameters;

    public function __construct(\Swift_Mailer $mailer, UrlGeneratorInterface $router, \Twig_Environment $twig, array $parameters) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->twig = $twig;
        $this->parameters = $parameters;
    }

    public function indexAction($name) {
        return $this->render('ASBEmailBundle:Default:index.html.twig', array('name' => $name));
    }

    /**
     * 
     * @param type $purpose
     * @param type $from
     * @param type $to
     * @param type $objectDisplay
     * @param type $context
     * @param type $cc
     * @param type $cci
     */
    public function sendMail($purpose, $from, $to, $objectDisplay, $context, $cc = NULL, $cci = NULL) {
        // get template
        $template = $this->_getTemplate($purpose);
        // Send email
        $success = $this->_sendEmail($from, $to, $objectDisplay, $context, $template, $cc, $cci);
        return $success;
    }

    /**
     * Get template for different purpose of mail
     * @param type $purpose
     * @return type
     */
    private function _getTemplate($purpose) {
        switch ($purpose) {
            case 'contactToAdmin':
                $template = $this->parameters['template']['contact']['admin'];
                break;
            case 'contactToUser':
                $template = $this->parameters['template']['contact']['user'];
                break;
            case 'newsletterToAdmin':
                $template = $this->parameters['template']['newsletter']['admin'];
                break;
            case 'newsletterToUser':
                $template = $this->parameters['template']['newsletter']['user'];
                break;

            default:
                break;
        }
        return $template;
    }

    /**
     * Function send email
     * @param type $from
     * @param type $to
     * @param type $objectDisplay   object which will be displayed in email
     * @param type $objectContent   the real object
     * @param type $contents
     * @param type $template
     */
    private function _sendEmail($from, $to, $objectDisplay, $contents, $template, $cc = NULL, $cci = NULL) {
        $template = $this->twig->loadTemplate($template);
//        $subject = $template->renderBlock('subject', $contents);
        $textBody = $template->renderBlock('body_text', $contents);
        $htmlBody = $template->renderBlock('body_html', $contents);

        $message = \Swift_Message::newInstance()
                ->setSubject($objectDisplay)
                ->setFrom($from)
                ->setTo($to);
        
        if ($cc != NULL) {
            $message->setCc($cc);
        }
        if ($cci != NULL) {
            $message->setBcc($cci);
        }


        if (!empty($htmlBody)) {
            $message->setBody($htmlBody, 'text/html')
                    ->addPart($textBody, 'text/plain');
        } else {
            $message->setBody($textBody);
        }

        try {
            // send Mail
            $this->mailer->send($message);
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

}
