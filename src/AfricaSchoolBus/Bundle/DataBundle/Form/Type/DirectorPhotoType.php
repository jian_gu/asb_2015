<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AfricaSchoolBus\Bundle\DataBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DirectorPhotoType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('photoFile', 'vich_image', array(
                    'label' => 'Photo du directeur',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                    'attr' => array(
                        'accept' => 'image/*',
                        'multiple' => FALSE
                    )
        ));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\DirectorPhoto',
        ));
    }

    public function getName() {
        return 'directorPhoto';
    }

}
