<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AfricaSchoolBus\Bundle\DataBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class AttachmentsTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        // TODO: Implement transform() method.
    }

    public function reverseTransform($files)
    {
        $attachments = [];
        foreach($files as $file){
            $attachment = new Attachment();
            $attachment->setFile($file);
            $attachments[] = $attachment;
        }
        return $attachments;
    }
}