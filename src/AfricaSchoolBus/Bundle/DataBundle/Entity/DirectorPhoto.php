<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * DirectorPhoto
 *
 * @ORM\Table(name="director_photo")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class DirectorPhoto {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="director_photo", fileNameProperty="photoName")
     * 
     * @var File $photoFile
     */
    private $photoFile;

    /**
     * @ORM\Column(type="string", length=255, name="photo_name", nullable=true)
     *
     * @var string $photoName
     */
    private $photoName;

    ///////////////////////
    ///// File upload /////
    ///////////////////////
    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $photo
     */
    public function setPhotoFile(File $photo = null) {
        $this->photoFile = $photo;

        if ($photo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            // $this->setUpdateAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getPhotoFile() {
        return $this->photoFile;
    }

    /**
     * Set photoName
     *
     * @param string $photoName
     * @return formation
     */
    public function setPhotoName($photoName) {
        $this->photoName = $photoName;

        return $this;
    }

    /**
     * Get photoName
     *
     * @return string 
     */
    public function getPhotoName() {
        return $this->photoName;
    }

    ////////////////////////////
    ///// Getter n setters /////
    ////////////////////////////
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

}
