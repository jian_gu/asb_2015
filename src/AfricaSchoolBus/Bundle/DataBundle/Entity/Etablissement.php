<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Etablissement
 *
 * @ORM\Table(name="etablissement")
 * @ORM\Entity(repositoryClass="AfricaSchoolBus\Bundle\DataBundle\Entity\EtablissementRepository")
 * @Vich\Uploadable
 */
class Etablissement {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TypeEtablissement")
     * @ORM\JoinColumn(name="type_etablissement_id", referencedColumnName="id")
     * */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="etablissement", type="string", length=128)
     */
    private $etablissement;

    /**
     * @var string
     *
     * @ORM\Column(name="rue", type="string", length=128, nullable=true)
     */
    private $rue;

    /**
     * @var string
     *
     * @ORM\Column(name="cp", type="string", length=16, nullable=true)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=128)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=64)
     */
    private $pays;    

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=32, nullable=true)
     */
    private $telephone;    

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=64)
     */
    private $siteWeb;    

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", options={"default" = 1})
     */
    private $activated;
    
    /**
     * @var decimal
     *
     * @ORM\Column(name="coefficient", type="decimal", options={"default" = 0})
     */
    private $coefficient;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="etablissement_logo", fileNameProperty="logoName")
     * 
     * @var File $logoFile
     */
    private $logoFile;

    /**
     * @ORM\Column(type="string", length=255, name="logo_name", nullable=true)
     *
     * @var string $logoName
     */
    private $logoName;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="update_at", type="datetime", nullable=true)
     */
    private $updateAt;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $logo
     */
    public function setLogoFile(File $logo = null) {
        $this->logoFile = $logo;

        if ($logo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        $this->setUpdateAt(new \DateTime('now'));
        }
    }

    /**
     * @return File
     */
    public function getLogoFile() {
        return $this->logoFile;
    }

    /**
     * Set logoName
     *
     * @param string $logoName
     * @return Etablissement
     */
    public function setLogoName($logoName) {
        $this->logoName = $logoName;

        return $this;
    }

    /**
     * Get logoName
     *
     * @return string 
     */
    public function getLogoName() {
        return $this->logoName;
    }
    
    // ...
    /**
     * @ORM\OneToMany(targetEntity="Formation", mappedBy="etablissement")
     **/
    private $formations;
    
    public function __construct() {
        $this->formations = new ArrayCollection();
    }
    
    public function __toString() {
        return (string) $this->getEtablissement();
    }

    //////////////////////////////
    ////// Getter n setters //////
    //////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set etablissement
     *
     * @param string $etablissement
     * @return Etablissement
     */
    public function setEtablissement($etablissement) {
        $this->etablissement = $etablissement;
        
        if($etablissement){
        $this->setUpdateAt(new \DateTime('now'));
        }

        return $this;
    }

    /**
     * Get etablissement
     *
     * @return string 
     */
    public function getEtablissement() {
        return $this->etablissement;
    }

    /**
     * Set rue
     *
     * @param string $rue
     * @return Etablissement
     */
    public function setRue($rue) {
        $this->rue = $rue;
        
        if($rue){
        $this->setUpdateAt(new \DateTime('now'));
        }
        return $this;
    }

    /**
     * Get rue
     *
     * @return string 
     */
    public function getRue() {
        return $this->rue;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return Etablissement
     */
    public function setCp($cp) {
        $this->cp = $cp;

        if($cp){
        $this->setUpdateAt(new \DateTime('now'));
        }
        return $this;
    }

    /**
     * Get cp
     *
     * @return string 
     */
    public function getCp() {
        return $this->cp;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Etablissement
     */
    public function setVille($ville) {
        $this->ville = $ville;

        if($ville){
        $this->setUpdateAt(new \DateTime('now'));
        }
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille() {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     * @return Etablissement
     */
    public function setPays($pays) {
        $this->pays = $pays;

        if($pays){
        $this->setUpdateAt(new \DateTime('now'));
        }
        return $this;
    }

    /**
     * Get pays
     *
     * @return string 
     */
    public function getPays() {
        return $this->pays;
    }

    /**
     * Set updateAt
     *
     * @param \DateTime $updateAt
     * @return Etablissement
     */
    public function setUpdateAt($updateAt) {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * Get updateAt
     *
     * @return \DateTime 
     */
    public function getUpdateAt() {
        return $this->updateAt;
    }

    /**
     * Set type
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\TypeEtablissement $type
     * @return Etablissement
     */
    public function setType(\AfricaSchoolBus\Bundle\DataBundle\Entity\TypeEtablissement $type = null) {
        $this->type = $type;

        if($type){
        $this->setUpdateAt(new \DateTime('now'));
        }
        return $this;
    }

    /**
     * Get type
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\TypeEtablissement 
     */
    public function getType() {
        return $this->type;
    }


    /**
     * Add formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     * @return Etablissement
     */
    public function addFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations[] = $formations;

        return $this;
    }

    /**
     * Remove formations
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations
     */
    public function removeFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formations)
    {
        $this->formations->removeElement($formations);
    }

    /**
     * Get formations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFormations()
    {
        return $this->formations;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     * @return Etablissement
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Get activated
     *
     * @return boolean 
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set coefficient
     *
     * @param string $coefficient
     * @return Etablissement
     */
    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    /**
     * Get coefficient
     *
     * @return string 
     */
    public function getCoefficient()
    {
        return $this->coefficient;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return Etablissement
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     * @return Etablissement
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string 
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }
}
