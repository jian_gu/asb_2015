<?php

namespace AfricaSchoolBus\Bundle\DataBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * ZoomFormation
 *
 * @ORM\Table(name="zoom_sur_fomation")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class ZoomSurFormation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Formation", cascade={"persist"})
     * @ORM\JoinColumn(name="zoom_sur_formation_formation_id", referencedColumnName="id")
     **/
    private $formation;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activated", type="boolean", options={"default" = 1})
     */
    private $activated;
    
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     * 
     * @Vich\UploadableField(mapping="zoom_sur_formation_photo", fileNameProperty="photoName")
     * 
     * @var File $photoFile
     */
    private $photoFile;

    /**
     * @ORM\Column(type="string", length=255, name="photo_name", nullable=true)
     *
     * @var string $photoName
     */
    private $photoName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $photo
     */
    public function setPhotoFile(File $photo = null) {
        $this->photoFile = $photo;

        if ($photo) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
        }
    }

    /**
     * @return File
     */
    public function getPhotoFile() {
        return $this->photoFile;
    }

    /**
     * Set photoName
     *
     * @param string $photoName
     * @return ZoomSurFormation
     */
    public function setPhotoName($photoName) {
        $this->photoName = $photoName;

        return $this;
    }

    /**
     * Get photoName
     *
     * @return string 
     */
    public function getPhotoName() {
        return $this->photoName;
    }
    
    ////////////////////////////////
    ////// Getters & setters ///////
    ////////////////////////////////
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return ZoomSurFormation
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set activated
     *
     * @param boolean $activated
     * @return ZoomSurFormation
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;

        return $this;
    }

    /**
     * Get activated
     *
     * @return boolean 
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set formation
     *
     * @param \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formation
     * @return ZoomSurFormation
     */
    public function setFormation(\AfricaSchoolBus\Bundle\DataBundle\Entity\Formation $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return \AfricaSchoolBus\Bundle\DataBundle\Entity\Formation 
     */
    public function getFormation()
    {
        return $this->formation;
    }
}
