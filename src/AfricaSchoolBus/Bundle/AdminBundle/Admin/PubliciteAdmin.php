<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PubliciteAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Général')
                ->add('sponsoriseePar', 'text', array('label' => 'Sponsorisée par'))
                ->add('titre', 'text', array('label' => 'Titre'))
                ->add('description', 'text', array('label' => 'Description'))
                ->add('lien', 'text', array('label' => 'Lien'))
                ->add('activated', null, array('label' => 'Activé',
                    'required' => false,))
                ->add('coefficient', 'integer', array('label' => 'Coefficient',
                    'required' => FALSE))
                ->end()
                ->with('Logo')
                ->add('logoFile', 'vich_image', array('label' => 'Logo de la pub',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
                ->end()
                ->with('Image')
                ->add('imageFile', 'vich_image', array('label' => 'Image de la pub',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
                ->end()
                ->with('Vidéo')
                ->add('videoFile', 'vich_image', array('label' => 'Vidéo de la pub',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
                ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('sponsoriseePar', null, array(
                    'label' => 'Sponsorisée par'
                ))
                ->add('titre')
                ->add('lien')
                ->add('coefficient')
                ->add('activated', null, array(
                    'label' => 'Activée'
                ))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('titre')
                ->add('sponsoriseePar', 'text', array(
                    'label' => 'Sponsorisée par'
                    ))
                ->add('lien')
                ->add('activated', 'boolean', array(
                    'label' => 'Activée',
                    'editable' => TRUE
                ))
                ->add('coefficient')
        ;
    }

}
