<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ZoomSurFormationAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('formation', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Formation',
                    'property' => 'nom'))
                ->add('text', 'textarea', array('label' => 'Texte', 'required'=> false))
                ->add('activated', NULL, array('label' => 'Activée'))
                ->add('photoFile', 'vich_image', array('label' => 'Photo à afficher',
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('formation')
                ->add('activated')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('id')
                ->add('formation')
                ->add('activated', 'boolean', array(
                    'label' => 'Activée',
                    'editable' => TRUE
                ))
        ;
    }

}
