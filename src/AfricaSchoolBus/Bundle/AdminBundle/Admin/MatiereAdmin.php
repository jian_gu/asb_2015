<?php

namespace AfricaSchoolBus\Bundle\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MatiereAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('domaine', 'entity', array('class' => 'AfricaSchoolBus\Bundle\DataBundle\Entity\Domaine',
                    'property' => 'domaine'))
                ->add('matiere', 'text', array('label' => 'Matière'))
                ->add('description', 'textarea', array('label' => 'Description',
                    'required' => FALSE))
                ->add('coefficient', 'integer', array('label' => 'Coefficient',
                    'required' => FALSE))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('domaine')
                ->add('matiere')
                ->add('coefficient')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('matiere')
                ->add('domaine')
                ->add('description')
                ->add('coefficient')
        ;
    }

}
