<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel {

    public function registerBundles() {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            // KNP bundles
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            //  MOPA bundles
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            // FOS bundles            
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\ElasticaBundle\FOSElasticaBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new FOS\RestBundle\FOSRestBundle(),
//            new FOS\CommentBundle\FOSCommentBundle(),
            // SONATA bundles
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\AdminBundle\SonataAdminBundle(),
            new Sonata\EasyExtendsBundle\SonataEasyExtendsBundle(),
            new Sonata\UserBundle\SonataUserBundle('FOSUserBundle'),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            // JMS bundles
            new JMS\SerializerBundle\JMSSerializerBundle(),
//            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
//            new JMS\AopBundle\JMSAopBundle(),
            // VICH bundles
            new Vich\UploaderBundle\VichUploaderBundle(),
            // ONEUP bundles
            new Oneup\FlysystemBundle\OneupFlysystemBundle(),
            // Genemu bundles
            new Genemu\Bundle\FormBundle\GenemuFormBundle(),
            // Application Bundles
            new Application\Sonata\UserBundle\ApplicationSonataUserBundle(),
//            new Application\FOS\CommentBundle\ApplicationFOSCommentBundle(),
            // AfricaSchoolBus bundles
            new AfricaSchoolBus\Bundle\UserBundle\ASBUserBundle(),
            new AfricaSchoolBus\Bundle\DataBundle\ASBDataBundle(),
            new AfricaSchoolBus\Bundle\AdminBundle\ASBAdminBundle(),
            new AfricaSchoolBus\Bundle\ViewBundle\ASBViewBundle(),
            new AfricaSchoolBus\Bundle\CommentBundle\ASBCommentBundle(),
            new AfricaSchoolBus\Bundle\EmailBundle\ASBEmailBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader) {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
